(function() {
	let styles = `
		.chatbox {
			padding: 0;
			height: 500px;
			margin-bottom: 0;
		}

		/* This wraps the text nicely slightly indented (there's padding on the left that is negated on the first line by text-indent) */
		.chatbox .chat-message {
			text-indent: -1em;
			padding: 0.3em 0.4em;
			padding-left: 1.4em; /* 1em (counter-indent) + 0.4em (the side padding) */
			word-break: break-word;
			line-height: 1.5;
		}

		.chatbox .chat-message * {
			text-indent: 0;
		}

		.chatbox .chat-message .username {
			vertical-align: baseline;
			margin: 0 2px;
			line-height: 15px; /* chat default */
			height: 22px;
			box-sizing: border-box;
		}

		.chatbox .chat-icon-container {
			display: inline-block;
			height: 0;
			vertical-align: top;
		}

		.chatbox .chat-message .chat-icon {
			margin: 0;
			width: auto;
			height: auto;
			max-width: 19px;
		}

		.chatbox .chat-message .chat-icon[src$="badge-10.png"],
		#player-list .player .chat-icon[src$="badge-10.png"] {
			width: 16px;
		}

		.chatbox .chat-message .chat-icon[src$="badge-4.png"],
		.chatbox .chat-message .chat-icon[src$="badge-5.png"] {
			margin-top: -3px;
		}

		/* Alternating chat line background colours */
		.chatbox .chat-message:nth-child(even) {
			background-color: #f8f8f8;
		}

		#chatbox-holder .chatbox .chat-message.highlighted {
			background-color: #FFF8D4;
		}

		.mcc-chatstyles-chatbox-resize-handle {
			height: 16px;
			cursor: n-resize;
		}

		#player-list .chat-icon-container {
			width: 19px;
			display: inline-block;
			text-align: center;
			vertical-align: top;
		}

		#player-list .player .chat-icon {
			width: auto;
			height: auto;
			max-width: 19px;
		}
	`;

	// Apply the stylesheet
	let elem = document.createElement('style');
	elem.innerHTML = styles;
	document.body.appendChild(elem);

	// Apply a custom chat message template
	let chatMessageTemplate = `
		<div class="chat-message">
			{{timeString}}
			<span class="username" style="background: {{background}}; color: {{name_color}};{{#fake}}border-radius:0px;{{/fake}}">
				{{#icon}}<span class="chat-icon-container"><img class="chat-icon" src="{{icon.src}}" alt="{{icon.alt}}" /></span>{{/icon}}
				{{username}}
			</span>
			{{message}}
		</div>
	`;

	document.getElementById('tpl-chat-message').innerHTML = chatMessageTemplate;

	// Apply a custom player list template
	let playerListTemplate = `
		<div class="player">
			<span class="chat-icon-container">{{#icon}}<img class="chat-icon" src="{{icon.src}}" alt="{{icon.alt}}" />{{/icon}}</span>
			{{username}}
		</div>
	`;

	document.getElementById('tpl-list-player').innerHTML = playerListTemplate;

	// Save mouse position for dragging etc.
	let mouse = {x: 0, y: 0}
	document.addEventListener('mousemove', (ev) => {
		mouse.x = ev.pageX;
		mouse.y = ev.pageY;
	});

	// Chatbox resizing by dragging
	// We create a stylesheet that we update; this assures chatboxes that might be added later are still affected (e.g. MCC+ PM box)
	let chatboxHeight = parseInt(localStorage.getItem('mcc-chatstyles-chatbox-height'), 10) || 500,
	    dragging = false,
	    mouseStartX = 0,
	    mouseStartY = 0,
	    tempHeight,
	    chatboxSizeStyles;
	
	chatboxSizeStyles = document.createElement('style');
	document.body.appendChild(chatboxSizeStyles);

	function applyChatboxHeight(height) {
		chatboxSizeStyles.innerHTML = `.chatbox { height: ${height}px }`;
	}

	applyChatboxHeight(chatboxHeight);

	function mouseDragLoop() {
		if (! dragging) return;

		let deltaX = mouse.x - mouseStartX,
		    deltaY = mouse.y - mouseStartY;

		tempHeight = chatboxHeight + deltaY;
		applyChatboxHeight(tempHeight);

		window.requestAnimationFrame(mouseDragLoop);
	}

	// Dragging handle
	let parent = document.querySelector('#chatbox-holder'),
	    handle = document.createElement('div'),
	    before = document.querySelector('#chat_msg-holder');

	handle.classList.add('mcc-chatstyles-chatbox-resize-handle');
	parent.insertBefore(handle, before);

	handle.addEventListener('mousedown', (ev) => {
		ev.preventDefault();
		if (dragging) return;
		if (parseInt(ev.button, 10) !== 0) return;

		dragging = true;
		mouseStartX = mouse.x;
		mouseStartY = mouse.y;

		mouseDragLoop();
	});

	document.addEventListener('mouseup', (ev) => {
		if (! dragging) return;
		ev.preventDefault();
		
		dragging = false;
		chatboxHeight = tempHeight;
		localStorage.setItem('mcc-chatstyles-chatbox-height', chatboxHeight);
	});

	// Highlights
	let client = window.io.connect('http://5.189.137.117:469'),
	    username = null,
	    lowerCaseUsername = null;

	client.on('loginAttemptResult', (data) => {
		console.log('loginAttemptResult', data);
		if (data.success) {
			username = data.username;
			lowerCaseUsername = data.username.toLowerCase();
		}
	});

	let generalChat = document.querySelector('#chatbox'),
	    groupChat = document.querySelector('#groupchatbox');

	let observer = new MutationObserver((mutations) => {
		console.log('lowerCaseUsername', lowerCaseUsername);
		mutations.forEach((mutation) => {
			console.log('mutation', mutation);
			if (mutation.type !== 'childList') return;

			[].slice.call(mutation.addedNodes).forEach(function(chatLine) {
				console.log('node', chatLine);
				let dummyNode = document.createElement('div');
				dummyNode.innerHTML = chatLine.innerHTML;

				let usernameElem = dummyNode.querySelector('.username');
				if (usernameElem) dummyNode.removeChild(usernameElem);

				console.log('dummyNode', dummyNode);

				if (dummyNode.innerHTML.toLowerCase().indexOf(lowerCaseUsername) !== -1) {
					chatLine.classList.add('highlighted');
				}
			});
		});
	});

	observer.observe(generalChat, {childList: true});
	observer.observe(groupChat, {childList: true});
})();
